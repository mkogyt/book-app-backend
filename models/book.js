'use strict'
module.exports = (sequelize, DataTypes) => {
  const Book = sequelize.define(
    'Book', 
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      book_title: DataTypes.STRING,
      author_name: DataTypes.STRING,
      category: DataTypes.INTEGER,
      isbn: DataTypes.STRING,
      img: DataTypes.STRING,
    }, 
    { 
      sequelize,
      modelName: "Book",
      timestamps: false
    }
  );

  return Book;
};