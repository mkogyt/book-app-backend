'use strict'

module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define(
    'Comment',
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true
      },
      bookId: DataTypes.INTEGER,
      assessment: DataTypes.INTEGER,
      author: DataTypes.STRING,
      title: DataTypes.STRING,
      text: DataTypes.STRING
    },
    {
      sequelize,
      modelName: 'Comment',
      timestamps: false
    }
  )

  Comment.associate = ({ Book, Comment }) => {
    Book.hasMany(Comment)
    Comment.belongsTo(Book)
  }

  return Comment
}
