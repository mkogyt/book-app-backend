const getAllBooks = require('./book/getAllBooks')
const createBook = require('./book/createBook')
const deleteBook = require('./book/deleteBook')
const updateBook = require('./book/updateBook')
const getBook = require('./book/getBook')

const getAllComments = require('./comment/getAllComments')
const createComment = require('./comment/createComment')
const deleteComment = require('./comment/deleteComment')
const updateComment = require('./comment/updateComment')
const getComment = require('./comment/getComment')

const resolvers = {
  Query: {
    getAllComments,
    getAllBooks,
    getBook: (_, { bookId }) => getBook(bookId),
    getComment: (_, { commentId }) => getComment(commentId)
  },
  Mutation: {
    createBook: (_, { input }) => createBook(input),
    deleteBook: (_, { bookId }) => deleteBook(bookId),
    updateBook: (_, { bookId, input }) => updateBook(bookId, input),
    createComment: (_, { input }) => createComment(input),
    deleteComment: (_, { commentId }) => deleteComment(commentId),
    updateComment: (_, { commentId, input }) => updateComment(commentId, input)
  }
}

module.exports = resolvers
