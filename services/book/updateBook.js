const { sequelize: { models } } = require('../../models')
const getBook = require('./getBook')

async function updateBook (bookId, input) {
  await models.Book.update(
    {
      book_title: input.book_title,
      author_name: input.author_name,
      category: input.category,
      isbn: input.isbn,
      img: input.img
    },
    { where: { id: bookId } }
  )

  return getBook(bookId)
}

module.exports = updateBook
