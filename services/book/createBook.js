const { sequelize: { models } } = require('../../models')
// const getBook = require('./getBook')

async function createBook (input) {
  const { dataValues } = await models.Book.create(input)

  return dataValues
}

module.exports = createBook
