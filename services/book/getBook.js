const { sequelize: { models } } = require('../../models')

async function getBook (bookId) {
  const book = await models.Book.findOne({ where: { id: bookId }, include: models.Comment })
  if (!book) {
    throw new Error('Book not found')
  } else {
    return book
  };
};

module.exports = getBook
