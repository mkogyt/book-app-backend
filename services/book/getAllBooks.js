const { sequelize: { models } } = require('../../models')

async function getAllBooks () {
  const books = await models.Book.findAll({ include: models.Comment })

  if (!books) {
    throw new Error('Books not found')
  } else {
    return books
  };
};

module.exports = getAllBooks
