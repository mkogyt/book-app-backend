const { sequelize: { models } } = require('../../models')

function deleteBook (bookId) {
  return models.Book.destroy({ where: { id: bookId } })
}

module.exports = deleteBook
