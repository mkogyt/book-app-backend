const { sequelize: { models } } = require('../../models')
const getComment = require('./getComment')

async function updateComment (commentId, input) {
  await models.Comment.update(
    {
      author: input.author,
      assessment: input.assessment,
      title: input.title,
      text: input.text
    },
    { where: { id: commentId } }
  )

  return getComment(commentId)
}

module.exports = updateComment
