const { sequelize: { models } } = require('../../models')

async function createComment (input) {
  const { dataValues } = await models.Comment.create(input)

  return dataValues
}

module.exports = createComment
