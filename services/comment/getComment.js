const { sequelize: { models } } = require('../../models')

async function getComment (commentId) {
  const comment = await models.Comment.findOne({
    where: { id: commentId },
    include: [
      {
        model: models.Book,
        as: 'Book'
      }
    ]
  })
  if (!comment) {
    throw new Error('Comment not found')
  } else {
    return comment
  };
};

module.exports = getComment
