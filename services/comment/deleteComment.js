const { sequelize: { models } } = require('../../models')

function deleteComment (commentId) {
  return models.Comment.destroy({ where: { id: commentId } })
}

module.exports = deleteComment
