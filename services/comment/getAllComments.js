const { sequelize: { models } } = require('../../models')

async function getAllComments () {
  const comments = await models.Comment.findAll()

  if (!comments) {
    throw new Error('Comments not found')
  } else {
    return comments
  };
};

module.exports = getAllComments
