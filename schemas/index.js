const { gql } = require('apollo-server')

const BookSchema = require('./book.graphql')
const CommentSchema = require('./comment.graphql')

const typeDefs = gql`
    type Query{
        _empty: String
    }
    type Mutation {
        _empty: String
    }
    ${BookSchema}
    ${CommentSchema}
`

module.exports = typeDefs
