const dotenv = require('dotenv')

dotenv.config({
  path: '.env'
})

module.exports = {
  development: {
    username: 'root',
    password: 'root',
    database: 'book',
    host: 'localhost',
    dialect: 'mysql',
    port: 8889
  },
  test: {
    username: 'root',
    password: 'root',
    database: 'book',
    host: '127.0.0.1',
    dialect: 'mysql'
  },
  production: {
    username: 'root',
    password: 'root',
    database: 'database_production',
    host: '127.0.0.1',
    dialect: 'mysql'
  }
}
