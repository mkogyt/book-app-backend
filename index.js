const express = require('express')
const dotenv = require('dotenv')
const cors = require('cors')
const { ApolloServer } = require('apollo-server')

const typeDefs = require('./schemas')
const resolvers = require('./services')

dotenv.config({
  path: './.env'
})

const app = express()

app.use(cors())

const server = new ApolloServer({ typeDefs, resolvers })

server.listen().then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`)
})
